# Changelog

#### v1.0.1

- Fix: modules from base-stdenv get an increased 'usage' count, because they are
  frequently imported with `import StdEnv` instead of directly. This was
  already implemented but broke when the library StdEnv was renamed to
  base-stdenv.

## v1.0.0

First tagged version
