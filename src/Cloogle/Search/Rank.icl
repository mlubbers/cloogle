implementation module Cloogle.Search.Rank

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 */

import StdArray
import StdBool
import StdInt
import StdList
import StdMisc
import StdOverloadedList
import StdReal
import StdString

import Clean.Types
import Data.Error
from Data.Foldable import class Foldable(foldr1())
from Data.Func import $
import Data.Functor
from Data.GenLexOrd import :: LexOrd, generic gLexOrd
import qualified Data.GenLexOrd
from Data.List import instance Foldable []
import qualified Data.Map
import Data.Tuple
import System.Process
import Text

import Cloogle.API
import Cloogle.DB
import Cloogle.Search

caf_rank_settings :: RankSettings
caf_rank_settings =:
	{ rs_matching_ngrams_q  = -1.0
	, rs_matching_ngrams_r  = -1.0
	, rs_record_field       = 1.0
	, rs_constructor        = 1.0
	, rs_unifier_n_types    = 1.0
	, rs_unifier_n_funcs    = 1.0
	, rs_unifier_n_conss    = 1.0
	, rs_unifier_n_args     = 1.0
	, rs_resolved_context   = 1.0
	, rs_unresolved_context = 1.0
	, rs_freevar_context    = 1.0
	, rs_module_usages      = -1.0
	}

setRankSettings :: !RankSettings -> (!Bool, !RankSettings)
setRankSettings rs = IF_INT_64_OR_32 setRankSettings64 setRankSettings32 rs
where
	setRankSettings64 :: !RankSettings -> (!Bool, !RankSettings)
	setRankSettings64 _ = code {
		fillcaf vcaf_rank_settings 0 12
		pushB TRUE
	}

	setRankSettings32 :: !RankSettings -> (!Bool, !RankSettings)
	setRankSettings32 _ = code {
		fillcaf vcaf_rank_settings 0 24
		pushB TRUE
	}

distance :: !CloogleEntry ![!Annotation!] -> ?Real
distance _ annots | Any (\a -> a=:ExactResult) annots = ?None
distance entry annots
	#! rs = caf_rank_settings
	#! info = symbolicDistance entry annots
	= ?Just $
		rs.rs_matching_ngrams_q  * info.rs_matching_ngrams_q +
		rs.rs_matching_ngrams_r  * info.rs_matching_ngrams_r +
		rs.rs_record_field       * info.rs_record_field +
		rs.rs_constructor        * info.rs_constructor +
		rs.rs_unifier_n_types    * info.rs_unifier_n_types +
		rs.rs_unifier_n_funcs    * info.rs_unifier_n_funcs +
		rs.rs_unifier_n_conss    * info.rs_unifier_n_conss +
		rs.rs_unifier_n_args     * info.rs_unifier_n_args +
		rs.rs_resolved_context   * info.rs_resolved_context +
		rs.rs_unresolved_context * info.rs_unresolved_context +
		rs.rs_freevar_context    * info.rs_freevar_context +
		rs.rs_module_usages      * info.rs_module_usages

symbolicDistance :: !CloogleEntry ![!Annotation!] -> RankInformation
symbolicDistance entry annots =
	{ rs_matching_ngrams_q  = case [r \\ MatchingNGramsQuery r <|- annots] of [r:_] -> r; _ -> 0.0
	, rs_matching_ngrams_r  = case [r \\ MatchingNGramsResult r <|- annots] of [r:_] -> r; _ -> 0.0
	, rs_record_field       = if entry=:(FunctionEntry {fe_kind=RecordField}) 1.0 0.0
	, rs_constructor        = if entry=:(FunctionEntry {fe_kind=Constructor}) 1.0 0.0
	, rs_unifier_n_types    = ntype
	, rs_unifier_n_funcs    = nfunc
	, rs_unifier_n_conss    = ncons
	, rs_unifier_n_args     = nargs
	, rs_resolved_context   = resolved_context
	, rs_unresolved_context = unresolved_context
	, rs_freevar_context    = freevar_context
	, rs_module_usages      = case moduleUsages entry of 0 -> 0.0; n -> log10 (toReal n)
	}
where
	(resolved_context,unresolved_context,freevar_context) = case [rc \\ RequiredContext rc <|- annots] of
		[rc]
			# (res,unres,onlyfv) = context_sizes 0 0 0 rc
			-> (toReal res, toReal unres, toReal onlyfv)
		_
			-> (0.0, 0.0, 0.0)
	where
		context_sizes :: !Int !Int !Int ![RequiredContext] -> (!Int, !Int, !Int)
		context_sizes res unres onlyfv [{only_free_vars,locations}:rest]
		| locations=:[]
			| only_free_vars
				= context_sizes res unres (onlyfv+1) rest
				= context_sizes res (unres+1) onlyfv rest
			= context_sizes (res+1) unres onlyfv rest
		context_sizes res unres onlyfv [] = (res,unres,onlyfv)

	(ntype,nfunc,ncons,nargs) = case [unifier_sizes u \\ Unifier u <|- annots] of
		[(nt,nf,nc,na):_] -> (toReal nt,toReal nf,toReal nc,toReal na)
		_                 -> (0.0,0.0,0.0,0.0)

	/**
	 * @result nr. of Type constructors
	 * @result nr. of Func constructors
	 * @result nr. of Cons constructors
	 * @result total nr. of arguments of Type and Cons constructors
	 */
	unifier_sizes :: !Unifier -> (!Int,!Int,!Int,!Int)
	unifier_sizes unif
	= count 0 0 0 0 [t \\ (_,t`) <- map fromUnifyingAssignment unif.assignments, t <- subtypes t`]
	where
		count :: !Int !Int !Int !Int ![Type] -> (!Int,!Int,!Int,!Int)
		count nt nf nc na []              = (nt,nf,nc,na)
		count nt nf nc na [Type _ as :ts] = count (nt+1) nf     nc     (na+length as) ts
		count nt nf nc na [Func _ _ _:ts] = count nt     (nf+1) nc     na             ts
		count nt nf nc na [Cons _ as :ts] = count nt     nf     (nc+1) (na+length as) ts
		count nt nf nc na [Var _     :ts] = count nt     nf     (nc+1) na             ts
		count nt nf nc na [_         :ts] = count nt     nf     nc     na             ts

match :: !UniqueResultIdentifier !CloogleEntry -> Bool
match (thing,mod,name) ce
| thing <> descriptorName ce = False
| otherwise = case getLocation ce of
	?Just (Location _ cemod _ _ _ cename) -> mod == cemod && name == cename
	?Just (Builtin cename _)              -> mod == "_builtin" && name == cename
	_                                     -> abort "error in match of UniqueResultIdentifier\n"
where
	descriptorName :: !CloogleEntry -> String
	descriptorName (FunctionEntry _)       = "Function"
	descriptorName (TypeDefEntry _)        = "TypeDef"
	descriptorName (ModuleEntry _)         = "Module"
	descriptorName (ClassEntry _)          = "Class"
	descriptorName (InstanceEntry _)       = "Instance"
	descriptorName (DeriveEntry _)         = "Derive"
	descriptorName (SyntaxEntry _)         = "Syntax"
	descriptorName (ABCInstructionEntry _) = "ABCInstruction"

findRankSettings :: ![(Request, RankConstraint)] !*CloogleDB !*World
	-> *(!MaybeError String RankSettings, *CloogleDB, !*World)
findRankSettings constraints cdb w
# (constraints,cdb) = rankConstraints constraints cdb
# (z3,w) = runProcessIO "z3" ["-in"] ?None w
| isError z3 = (Error "Failed to run z3", cdb, w)
# (z3h,z3io) = fromOk z3
# z3input = join "\n" (constraints ++ ["(set-option :pp.decimal true)","(check-sat)","(get-model)","(exit)"]) +++ "\n"
# (err,w) = writePipe z3input z3io.stdIn w
| isError err = (Error "Failed to write constraints to z3", cdb, w)
# (rcode,w) = waitForProcess z3h w
| isError rcode || fromOk rcode <> 0
	= (Error ("z3 failed to compute a model with these constraints:\n" +++ z3input), cdb, w)
# (out,w) = readPipeBlocking z3io.stdOut w
| isError out = (Error "Failed to read z3 output", cdb, w)
# out = split "\n" $ fromOk out
# settings = findSettings out
	{ rs_matching_ngrams_q  = 0.0
	, rs_matching_ngrams_r  = 0.0
	, rs_record_field       = 0.0
	, rs_constructor        = 0.0
	, rs_unifier_n_types    = 0.0
	, rs_unifier_n_funcs    = 0.0
	, rs_unifier_n_conss    = 0.0
	, rs_unifier_n_args     = 0.0
	, rs_resolved_context   = 0.0
	, rs_unresolved_context = 0.0
	, rs_freevar_context    = 0.0
	, rs_module_usages      = 0.0
	}
= (Ok settings, cdb, w)
where
	findSettings :: ![String] !RankSettings -> RankSettings
	findSettings [s:v:ss] rs
	| startsWith "  (define-fun " s
		# name = s % (14,size s-9) // strip off '  (define-fun ' and ' () Real'
		# val = toReal {#c \\ c <-: v | isDigit c || c == '.' || c == '-'}
		# rs = case name of
			"rs_matching_ngrams_q"  -> {rs & rs_matching_ngrams_q =val}
			"rs_matching_ngrams_r"  -> {rs & rs_matching_ngrams_r =val}
			"rs_record_field"       -> {rs & rs_record_field      =val}
			"rs_constructor"        -> {rs & rs_constructor       =val}
			"rs_unifier_n_types"    -> {rs & rs_unifier_n_types   =val}
			"rs_unifier_n_funcs"    -> {rs & rs_unifier_n_funcs   =val}
			"rs_unifier_n_conss"    -> {rs & rs_unifier_n_conss   =val}
			"rs_unifier_n_args"     -> {rs & rs_unifier_n_args    =val}
			"rs_resolved_context"   -> {rs & rs_resolved_context  =val}
			"rs_unresolved_context" -> {rs & rs_unresolved_context=val}
			"rs_freevar_context"    -> {rs & rs_freevar_context   =val}
			"rs_module_usages"      -> {rs & rs_module_usages     =val}
			_                       -> abort ("unknown setting " +++ name +++ "\n")
		= findSettings ss rs
	findSettings [s:ss] rs = findSettings ss rs
	findSettings [] rs = rs

rankConstraints :: ![(Request, RankConstraint)] !*CloogleDB -> *([String], *CloogleDB)
rankConstraints constraints cdb
# (constraints,cdb) = findConstraints constraints 'Data.Map'.newMap cdb
= (default ++ constraints,cdb)
where
	/* When the constraints are not specific enough, it may happen accidentally
	 * that weights go in the unexpected direction, e.g. that a large unifier
	 * causes a smaller distance or that a small difference in names causes a
	 * larger distance. To prevent this from happening we assert whether each
	 * weight should be non-negative or non-positive. */
	default =
		[ "(declare-const rs_matching_ngrams_q  Real) (assert (<= rs_matching_ngrams_q  0))"
		, "(declare-const rs_matching_ngrams_r  Real) (assert (<= rs_matching_ngrams_q  0))"
		, "(declare-const rs_record_field       Real) (assert (>= rs_record_field       0))"
		, "(declare-const rs_constructor        Real) (assert (>= rs_constructor        0))"
		, "(declare-const rs_unifier_n_types    Real) (assert (>= rs_unifier_n_types    0))"
		, "(declare-const rs_unifier_n_funcs    Real) (assert (>= rs_unifier_n_funcs    0))"
		, "(declare-const rs_unifier_n_conss    Real) (assert (>= rs_unifier_n_conss    0))"
		, "(declare-const rs_unifier_n_args     Real) (assert (>= rs_unifier_n_args     0))"
		, "(declare-const rs_resolved_context   Real) (assert (>= rs_resolved_context   0))"
		, "(declare-const rs_unresolved_context Real) (assert (>= rs_unresolved_context 0))"
		, "(declare-const rs_freevar_context    Real) (assert (>= rs_freevar_context    0))"
		, "(declare-const rs_module_usages      Real) (assert (<= rs_module_usages      0))"
		]

derive gLexOrd Request, ?
instance < Request where (<) a b = (gLexOrd{|*|} a b)=:'Data.GenLexOrd'.LT

findConstraints ::
	![(Request, RankConstraint)]
	!(Map Request (! ?Type,!Map String [TypeDef],![TypeDef],![!(CloogleEntry,[!Annotation!])!]))
	!*CloogleDB -> *([String], *CloogleDB)
findConstraints [(req,LT urid1 urid2):rest] results cdb
# (orgsearchtype,allsyns,usedsyns,entries,cdb) = case 'Data.Map'.get req results of
	?Just (t,as,us,es) -> (t,as,us,es,cdb)
	_                  -> search` req cdb
# results = 'Data.Map'.put req (orgsearchtype,allsyns,usedsyns,entries) results
# (e1,annots1,cdb) = findEntry orgsearchtype allsyns usedsyns urid1 entries cdb
# (e2,annots2,cdb) = findEntry orgsearchtype allsyns usedsyns urid2 entries cdb
# ri1 = symbolicDistance e1 annots1
# ri2 = symbolicDistance e2 annots2
# this = "(assert (< (" +++ formula ri1 +++ ") (" +++ formula ri2 +++ ")))"
# cdb = resetDB cdb
# (rest,cdb) = findConstraints rest results cdb
= ([this:rest],cdb)
where
	findEntry orgsearchtype allsyns usedsyns urid=:(_,mod,name) entries cdb
		= case Filter (\(e,_) -> match urid e) entries of
			[|(e1=:FunctionEntry fe,annots):[|]]
				# (unif,usedsyns,required_context,cdb) = unifyInformation orgsearchtype allsyns usedsyns fe cdb
				# annots = [!RequiredContext required_context,UsedSynonyms (length usedsyns):annots!]
				# annots = case unif of
					?Just unif -> [!Unifier unif:annots!]
					?None      -> annots
				-> (e1,annots,cdb)
			[|(e1,annots):[|]]
				-> (e1,annots,cdb)
			[|] -> abort ("no match for URID " +++ mod +++ "." +++ name +++ "\n")
			_   -> abort ("too many matches for URID " +++ mod +++ "." +++ name +++ "\n")

	formula :: !RankInformation -> String
	formula ri = sum
		[ "* rs_matching_ngrams_q "  <+ ri.rs_matching_ngrams_q
		, "* rs_matching_ngrams_r "  <+ ri.rs_matching_ngrams_r
		, "* rs_record_field "       <+ ri.rs_record_field
		, "* rs_constructor "        <+ ri.rs_constructor
		, "* rs_unifier_n_types "    <+ ri.rs_unifier_n_types
		, "* rs_unifier_n_funcs "    <+ ri.rs_unifier_n_funcs
		, "* rs_unifier_n_conss "    <+ ri.rs_unifier_n_conss
		, "* rs_unifier_n_args "     <+ ri.rs_unifier_n_args
		, "* rs_resolved_context "   <+ ri.rs_resolved_context
		, "* rs_unresolved_context " <+ ri.rs_unresolved_context
		, "* rs_freevar_context "    <+ ri.rs_freevar_context
		, "* rs_module_usages "      <+ ri.rs_module_usages
		]
	where
		sum :: [String] -> String
		sum [t] = t
		sum [t:ts]
		# s = sum ts
		= "+ (" +++ t +++ ") (" +++ s +++ ")"
		sum [] = abort "error in findConstraints\n"
findConstraints [] _ cdb = ([],cdb)
