definition module Cloogle.Search.Rank

/**
 * Functions to give a distance measure to a search result.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of Cloogle.
 *
 * Cloogle is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Cloogle. If not, see <https://www.gnu.org/licenses/>.
 */

from Clean.Types import :: TypeDef
from Data.Error import :: MaybeError

from Cloogle.API import :: Request, :: LocationResult, :: FunctionKind
from Cloogle.DB import :: Annotation, :: CloogleEntry, :: CloogleDB

/**
 * A rank is computed as the weighted sum of various metrics. The coefficients
 * are given by this record.
 */
:: RankSettings =
	{ rs_matching_ngrams_q  :: !Real //* ratio of matching query n-grams
	, rs_matching_ngrams_r  :: !Real //* nr. of matching n-grams divided by the size of the result

	, rs_record_field       :: !Real //* record fields
	, rs_constructor        :: !Real //* constructors

	, rs_unifier_n_types    :: !Real //* number of Type constructors in the unifier
	, rs_unifier_n_funcs    :: !Real //* number of Func constructors in the unifier
	, rs_unifier_n_conss    :: !Real //* number of Cons constructors in the unifier
	, rs_unifier_n_args     :: !Real //* number of arguments of Type and Cons in the unifier

	, rs_resolved_context   :: !Real //* class contexts with known instances
	, rs_unresolved_context :: !Real //* class contexts without known instances
	, rs_freevar_context    :: !Real //* number of free variables in class contexts

	, rs_module_usages      :: !Real //* nr. of usages (imports) of the module of the entry
	}

/**
 * The rank settings are kept in a CAF. This impure function overwrites the CAF
 * so that the rank settings can be set up on start-up. The return value is
 * always True.
 */
setRankSettings :: !RankSettings -> (!Bool, !RankSettings)

/**
 * This record is the same as {{`RankSettings`}}, but the members are
 * interpreted as the values rather than the weights.
 */
:: RankInformation :== RankSettings

distance :: !CloogleEntry ![!Annotation!] -> ?Real

symbolicDistance :: !CloogleEntry ![!Annotation!] -> RankInformation

:: RankConstraint
	= LT !UniqueResultIdentifier !UniqueResultIdentifier //* arg1 should have lower distance than arg2

/**
 * @representation kind (e.g. Function for FunctionEntry), module name, name of element
 */
:: UniqueResultIdentifier :== (!String, !String, !String)

/**
 * Find suitable ranking settings given a set of constraints. This requires Z3
 * to be installed on the system.
 */
findRankSettings :: ![(Request, RankConstraint)] !*CloogleDB !*World
	-> *(!MaybeError String RankSettings, *CloogleDB, !*World)
